// SPDX-License-Identifier: GPL-3.0-or-later
/*
 * Copyright (C) 2022 Hu Jialun
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without
 * even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.

 * You should have received a copy of the GNU General Public License along with this program. If
 * not, see <https://www.gnu.org/licenses/>.
 */

#include <libebackend/libebackend.h>
#include <shell/e-shell.h>

typedef struct _EPersist {
	EExtension parent;
	GDBusConnection *connection;
} EPersist;

/*
 * EPersistClass:
 * @workaround_needed: whether the monitoring workaround for secondary instance is needed.
 * See Evolution issue #1783
 */
typedef struct _EPersistClass {
	EExtensionClass parent_class;
	gboolean workaround_needed; // made as class variable since it never changes in one run
} EPersistClass;

#define E_TYPE_PERSIST (e_persist_get_type ())
#define E_PERSIST(obj) (G_TYPE_CHECK_INSTANCE_CAST ((obj), E_TYPE_PERSIST, EPersist))
#define E_PERSIST_CLASS(klass) (G_TYPE_CHECK_CLASS_CAST ((klass), E_TYPE_PERSIST, EPersistClass))
#define E_IS_PERSIST(obj) (G_TYPE_CHECK_INSTANCE_TYPE ((obj), E_TYPE_PERSIST))
#define E_IS_PERSIST_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), E_TYPE_PERSIST))
#define E_PERSIST_GET_CLASS(obj) (G_TYPE_INSTANCE_GET_CLASS ((obj), E_TYPE_PERSIST, EPersistClass))

G_DEFINE_DYNAMIC_TYPE (EPersist, e_persist, E_TYPE_EXTENSION)

static void
e_shell_quit_requested_cb (EShell *shell, EShellQuitReason reason, gpointer data)
{
	if (reason != E_SHELL_QUIT_LAST_WINDOW) {
		return;
	}
	e_shell_cancel_quit (shell);
	gtk_widget_hide (GTK_WIDGET (e_shell_get_active_window (shell)));
	// XXX seems that hold is not needed here to prevent it from exiting
	g_debug ("Hid window");
}

static void
g_application_activate_cb (GApplication *app, gpointer data)
{
	g_return_if_fail (e_shell_get_active_window (E_SHELL (app)) != NULL);
	gtk_widget_show (GTK_WIDGET (e_shell_get_active_window (E_SHELL (app))));
	g_debug ("Showed window");
}

static gboolean
default_e_shell_emit_activate (gpointer user_data)
{
	g_return_val_if_fail (e_shell_get_default () != NULL, FALSE);
	g_signal_emit_by_name (e_shell_get_default (), "activate");
	return FALSE;
}

static GDBusMessage *
g_dbus_message_filter (GDBusConnection *connection, GDBusMessage *message, gboolean incoming,
		       gpointer user_data)
{
	// filters are run in a separate thread so no direct calling GTK functions here
	g_debug ("Intercepted DBus monitoring message");
	if (!incoming) {
		// XXX for some reason an NameLost signal is directed here
		// XXX would our own RequestName be directed here under some circumstance as well?
		g_debug ("Unsolicited message received, ignoring.");
		goto out;
	}
	g_idle_add (G_SOURCE_FUNC (default_e_shell_emit_activate), NULL);
out: // cleanup: these messages will not be consumed and has to be unreferenced
	g_object_unref (message);
	return NULL;
}

static gboolean
workaround_activate (EPersist *instance, GError **error)
{
	gchar *address = g_dbus_address_get_for_bus_sync (G_BUS_TYPE_SESSION, NULL, error);
	if (address == NULL) {
		return FALSE;
	}
	g_debug ("Address: %s", address);
	instance->connection = g_dbus_connection_new_for_address_sync (
		address,
		G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT
			| G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION,
		NULL, NULL, error);
	if (instance->connection == NULL) {
		return FALSE;
	}
	GVariantBuilder *builder = g_variant_builder_new (G_VARIANT_TYPE ("as"));
	g_variant_builder_add_value (builder,
				     g_variant_new_string ("type=method_call,"
							   "member='RequestName',"
							   "arg0='org.gnome.Evolution',"
							   "path='/org/freedesktop/DBus',"
							   "destination='org.freedesktop.DBus',"
							   "interface='org.freedesktop.DBus'"));
	if (g_dbus_connection_call_sync (instance->connection, "org.freedesktop.DBus",
					 "/org/freedesktop/DBus", "org.freedesktop.DBus.Monitoring",
					 "BecomeMonitor", g_variant_new ("(asu)", builder, 0), NULL,
					 G_DBUS_CALL_FLAGS_NONE, G_MAXINT, NULL, error)
	    == NULL) {
		return FALSE;
	}
	g_dbus_connection_add_filter (instance->connection, g_dbus_message_filter, NULL, NULL);
	return TRUE;
}

static void
e_persist_constructed (GObject *object)
{
	EExtensible *extensible = e_extension_get_extensible (E_EXTENSION (object));
	int id1 = g_signal_connect (extensible, "quit-requested",
				    G_CALLBACK (e_shell_quit_requested_cb), NULL);
	int id2 = g_signal_connect (extensible, "activate", G_CALLBACK (g_application_activate_cb),
				    NULL);
	g_return_if_fail (id1 != 0 && id2 != 0);
	EPersist *instance = E_PERSIST (object);
	GError *error = NULL;
	if (E_PERSIST_GET_CLASS (instance)->workaround_needed) {
		if (workaround_activate (instance, &error)) {
			g_warning ("Failed to use the DBus monitoring workaround: %s",
				   error->message);
		}
	}
}

static void
e_persist_finalize (GObject *object)
{
	GError *error = NULL;
	EPersist *instance = E_PERSIST (object);
	if (E_PERSIST_GET_CLASS (instance)->workaround_needed && instance->connection != NULL) {
		g_dbus_connection_close_sync (instance->connection, NULL, &error);
		g_object_unref (instance->connection);
	}
	G_OBJECT_CLASS (e_persist_parent_class)->finalize (object);
}

static void
e_persist_class_init (EPersistClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	EExtensionClass *extension_class = E_EXTENSION_CLASS (klass);
	object_class->constructed = e_persist_constructed;
	object_class->finalize = e_persist_finalize;
	extension_class->extensible_type = E_TYPE_SHELL;

	GSettings *settings = e_util_ref_settings ("org.gnome.evolution");
	gchar *version_string = g_settings_get_string (settings, "version");
	// NOLINTNEXTLINE(readability-isolate-declaration, cppcoreguidelines-init-variables)
	gint major, minor, micro;
	// NOLINTNEXTLINE(cert-err34-c)
	int conv_count = sscanf (version_string, "%d.%d.%d", &major, &minor, &micro);
	g_assert_cmpint (conv_count, ==, 3);
	klass->workaround_needed = (major < 3)
				   || (major == 3
				       && ((minor < 42) || (minor == 42 && micro < 4)
					   || (minor == 43 && micro < 2)));
	g_debug ("Version %s, workaround %s needed", version_string,
		 klass->workaround_needed ? "is" : "not");
	g_free (version_string);
	g_object_unref (settings);
}

static void
e_persist_class_finalize (EPersistClass *klass)
{
}

static void
e_persist_init (EPersist *extension)
{
	/* The EShell object we're extending is not available yet,
	 * but we could still do some early initialization here. */
}

// exposed extension interface

G_GNUC_UNUSED
G_MODULE_EXPORT void
e_module_load (GTypeModule *type_module)
{
	e_persist_register_type (type_module);
}

G_GNUC_UNUSED
G_MODULE_EXPORT void
e_module_unload (GTypeModule *type_module)
{
}
