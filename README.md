# evolution-persist

**`evolution-persist`** is an Evolution extension which keeps Evolution running in the background for notifications to
be delivered on time.

## Build & Installation

CMake is required for building this project.

It is possible to install at a different location than the global module directory
(e.g. `~/.local/share/evolution/modules/`) by passing CMake a custom prefix.

## Internals

The extension functions by hooking onto
- the `EShell::quit-requested` signal to intercept exits caused by the closing of the last window.
- the `GApplication::activate` signal, which is emitted on the primary instance when another remote one is spawned.
  
  Due to a discovered bug of Evolution along the way, the extension also works around it by (ab)using a DBus monitor
  connection to directly detect the `RequestName` call from the remote instance.

## Motivation & Rationale

It is often taken for granted that e-mail clients should be able to receive and push notifications in the background
even without an open window. However, this imperative need is lacking in Evolution and users are instead advised to keep
a window open in order not to miss emails. Proposals seem to be repeatedly raised to no avail.

There was the `evolution-on` project, which had similar objectives and serves as the giant's shoulder. Nonetheless,
- Even its newest fork has gone unmaintained for years.
- It is an [Evolution *plugin*](https://wiki.gnome.org/Apps/Evolution/EPlugin), which is a deprecated way to extend
  Evolution functionalities.
- It can only minimise to the tray, which is deprecated for GNOME 3.

While the tray has been deprecated, it is justified that Evolution run in the background, quoting the [GNOME status icon
migration guidelines](https://wiki.gnome.org/Initiatives/StatusIconMigration/Guidelines#Running_in_the_background):

> In some cases it is appropriate for an application to continue to run after all its windows have been closed. This can
> include chat and messaging applications, reader applications that alert users about new content, or scheduling or
> organisation apps that show reminders. Status icons have traditionally been used for this purpose. [...] Applications
> that want to continue to run after their windows have been closed should simply continue to run.